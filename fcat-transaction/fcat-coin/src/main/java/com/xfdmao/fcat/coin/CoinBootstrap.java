package com.xfdmao.fcat.coin;

import com.xfdmao.fcat.coin.controller.KlineController;
import com.xfdmao.fcat.coin.controller.TransactionController;
import com.xfdmao.fcat.coin.huobi.websocket.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Created by fier on 2018/09/20
 */
@SpringBootApplication
@ServletComponentScan("com.xfdmao.fcat.coin.config.druid")
@EnableAsync
public class CoinBootstrap  implements CommandLineRunner {
    String protocol = "wss://";
    String host = "api.btcgateway.pro";
    String port ="80";
    String aO = "/ws";
    String market = "/notification";
    String accessKey = "";
    String secretKey = "";

    @Autowired
    Client client;

    @Autowired
    private TransactionController transactionController;

    @Autowired
    private KlineController klineController;


    public static void main(String[] args) {
        SpringApplication.run(CoinBootstrap.class, args);
    }
    @Override
    public void run(String... strings) throws Exception {

        //异步运行-实时交易
        transactionController.realTransaction();
        //异步运行-获取历史k线并存储
        klineController.queryHistoryKlineAndSave();
        //异步运行-存储制定好的策略结果值写到字典表
        klineController.saveBuySellStrategy();

/*      连接socket
                try {

                URI uri = new URI(protocol + host + ":" + port + market);
                System.out.println(protocol + host + ":" + port + market + "  1");
                System.out.println(uri.getHost() + uri.getPath());
              // TODO 合约的websocket暂时不启用
              WebSocketClient ws = new WebSocketAccountsAndOrders(uri, accessKey, secretKey);
                client.connect(ws);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }*/
    }
}
 