package com.xfdmao.fcat.coin.base.entity;

import com.qwrt.base.common.util.DateUtil;
import lombok.Data;

import java.util.Date;

/**
 * Created by cissa on 2019/7/27.
 */
public class KlineInfo implements  Cloneable{
    private Date date;
    private double open;
    private double high;
    private double low;
    private double close;
    private double volume;
    /**
     * 上引线跌幅
     */
    private double upLeadGain;

    /**
     * 下引线涨幅
     */
    private double downLeadGain;

    /**
     * 当前K线相比上一根K线的量能比
     */
    private double volRate;

    /**
     * k线涨幅
     */
    private double gain;

    /**
     * 买卖状态
     */
    private String buySellStatus;
    /**
     * 收益率
     */
    private double incomeRate;

    /**
     * 今日开盘价
     */
    private double dayOpen;

    /**
     * 操作类型
     */
    private String  optType;

    /**
     * 开仓到平仓之间的最高涨幅
     */
    private double highGain;

    /**
     * 在持仓周期内收益最高的K线
     */
    private boolean highGainFlag;

    /**
     *最佳持仓K线个数
     */
    private int bestHoldKlineNum;

    /**
     * 开仓前20根K线的涨幅总和
     */
    private double before20Gain;

    /**
     * 开仓前20根k线上引线涨幅总和
     */
    private double before20UpLeadGain;

    /**
     * 开仓前20根k线下引线涨幅总和
     */
    private double before20DownLeadGain;

    /**
     * 长期均线的值
     */
    private double upAvgPrice;

    /**
     * 短期均线的值
     */
    private double downAvgPrice;

    /**
     * 量能比1:最近5根K线的量能占最近20根K线量能的比率
     */
    private double volRate1;


    public double getUpAvgPrice() {
        return upAvgPrice;
    }

    public void setUpAvgPrice(double upAvgPrice) {
        this.upAvgPrice = upAvgPrice;
    }

    public double getDownAvgPrice() {
        return downAvgPrice;
    }

    public void setDownAvgPrice(double downAvgPrice) {
        this.downAvgPrice = downAvgPrice;
    }

    public double getBefore20Gain() {
        return before20Gain;
    }

    public void setBefore20Gain(double before20Gain) {
        this.before20Gain = before20Gain;
    }

    public double getBefore20UpLeadGain() {
        return before20UpLeadGain;
    }

    public void setBefore20UpLeadGain(double before20UpLeadGain) {
        this.before20UpLeadGain = before20UpLeadGain;
    }

    public double getBefore20DownLeadGain() {
        return before20DownLeadGain;
    }

    public void setBefore20DownLeadGain(double before20DownLeadGain) {
        this.before20DownLeadGain = before20DownLeadGain;
    }

    public int getBestHoldKlineNum() {
        return bestHoldKlineNum;
    }

    public void setBestHoldKlineNum(int bestHoldKlineNum) {
        this.bestHoldKlineNum = bestHoldKlineNum;
    }

    public boolean isHighGainFlag() {
        return highGainFlag;
    }

    public void setHighGainFlag(boolean highGainFlag) {
        this.highGainFlag = highGainFlag;
    }

    public double getHighGain() {
        return highGain;
    }

    public void setHighGain(double highGain) {
        this.highGain = highGain;
    }

    public String getOptType() {
        return optType;
    }

    public void setOptType(String optType) {
        this.optType = optType;
    }

    public double getDayOpen() {
        return dayOpen;
    }

    public void setDayOpen(double dayOpen) {
        this.dayOpen = dayOpen;
    }

    public double getUpLeadGain() {
        return upLeadGain;
    }

    public void setUpLeadGain(double upLeadGain) {
        this.upLeadGain = upLeadGain;
    }

    public double getDownLeadGain() {
        return downLeadGain;
    }

    public void setDownLeadGain(double downLeadGain) {
        this.downLeadGain = downLeadGain;
    }

    public double getVolRate() {
        return volRate;
    }

    public void setVolRate(double volRate) {
        this.volRate = volRate;
    }

    public double getIncomeRate() {
        return incomeRate;
    }

    public void setIncomeRate(double incomeRate) {
        this.incomeRate = incomeRate;
    }

    public String getBuySellStatus() {
        return buySellStatus;
    }

    public void setBuySellStatus(String buySellStatus) {
        this.buySellStatus = buySellStatus;
    }

    public double getGain() {
        return gain;
    }

    public void setGain(double gain) {
        this.gain = gain;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "KlineInfo{" +
                "date=" + DateUtil.formatDate(date,DateUtil.TIME_PATTERN_DAY_SLASH) +
                ", open=" + open +
                ", high=" + high +
                ", low=" + low +
                ", close=" + close +
                ", volume=" + volume +
                ", gain=" + gain +
                ", buySellStatus='" + buySellStatus + '\'' +
                ", incomeRate=" + incomeRate +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
