package com.xfdmao.fcat.coin.service;

import com.qwrt.base.common.service.BaseService;
import com.xfdmao.fcat.coin.entity.TDict;

/**
 * Created by fier on 2018/09/20
 */
public interface TDictService extends BaseService<TDict>{
}
