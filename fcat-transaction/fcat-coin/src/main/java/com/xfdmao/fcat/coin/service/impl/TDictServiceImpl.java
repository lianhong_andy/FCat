package com.xfdmao.fcat.coin.service.impl;

import com.qwrt.base.common.service.impl.BaseServiceImpl;
import com.xfdmao.fcat.coin.entity.TDict;
import com.xfdmao.fcat.coin.mapper.TDictMapper;
import com.xfdmao.fcat.coin.service.TDictService;
import org.springframework.stereotype.Service;

/**
 * Created by fier on 2018/09/20
 */
@Service
public class TDictServiceImpl extends BaseServiceImpl<TDictMapper,TDict> implements TDictService{
}
