package com.xfdmao.fcat.coin.util;

import com.qwrt.base.common.util.GeneratorUtil;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

public class Genterator {

    public static void main(String[] args) {
        String modelPath=getEntityAbsolutePath(getCurrentAbsolutePath());
        GeneratorUtil.generatorService(modelPath);
        modelPath = modelPath + "/";
        GeneratorUtil.generatorController(modelPath);
    }

    static String getEntityAbsolutePath(String absolutePath){
        int index=absolutePath.lastIndexOf("\\");
        String path=absolutePath.substring(0,index+1)+ "entity";
        return path.replaceAll("\\\\","/");
    }
    static String getCurrentAbsolutePath(){
        File directory = new File("");//设定为当前文件夹
        String projectPath=directory.getAbsolutePath();
        int index=projectPath.lastIndexOf("\\");
        String lastValue=projectPath.substring(index+1);
        String absolutePath= null;
        try {
            absolutePath = ResourceUtils.getURL("classpath:").getPath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String packageName = Genterator.class.getPackage().getName();
        packageName=packageName.replace('.','\\');
        String classessPath=absolutePath.substring(absolutePath.lastIndexOf(lastValue)+lastValue.length());
        classessPath=classessPath.replaceAll("/","\\\\");
        String path=projectPath+classessPath.substring(0,classessPath.indexOf("target\\classes"))+"src\\main\\java\\"+packageName;
        return path;
    }
}
