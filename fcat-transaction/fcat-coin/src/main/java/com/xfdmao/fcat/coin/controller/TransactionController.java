package com.xfdmao.fcat.coin.controller;

import com.alibaba.fastjson.JSONObject;
import com.xfdmao.fcat.coin.entity.Strategy;
import com.xfdmao.fcat.coin.entity.StrategyUser;
import com.xfdmao.fcat.coin.entity.Token;
import com.xfdmao.fcat.coin.huobi.util.StrategyUtil;
import com.xfdmao.fcat.coin.service.StrategyService;
import com.xfdmao.fcat.coin.service.StrategyUserService;
import com.xfdmao.fcat.coin.service.TokenService;
import com.qwrt.base.common.util.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by fier on 2018/10/31.
 */
@RestController
@RequestMapping("transaction")
public class TransactionController {
    private static Logger logger = LoggerFactory.getLogger(TransactionController.class);

    @Value("${spring.profiles.active}")
    public String active;

    /**
     * // 合约接口地址 "https://api.hbdm.com";//火币api接口地址https://api.hbdm.com;
     */
    @Value("${huobi.contract.url_prex}")
    public String url_prex;

    @Autowired
    private StrategyService strategyService;

    @Autowired
    private StrategyUserService strategyUserService;

    @Autowired
    private TokenService tokenService;

    @GetMapping(value = "/realTransaction")
    public JSONObject realTransaction(@RequestParam("strategyUserId") Long strategyUserId) {
        StrategyUser strategyUser = strategyUserService.selectById(strategyUserId);
        Strategy strategy = strategyService.selectById(strategyUser.getStrategyId());
        Token token = new Token();
        token.setUsername(strategyUser.getUsername());
        token = tokenService.selectOne(token);
        try {
            if ("BTC".equals(strategy.getSymbol())) {
                if ("buy".equals(strategy.getBuySell())) {
                    StrategyUtil.buyBTC(url_prex,strategy, strategyUser, token);
                }else if("sell".equals(strategy.getBuySell())){
                    StrategyUtil.sellBTC(url_prex,strategy, strategyUser, token);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return JsonUtil.getFailJsonObject(e.getMessage());
        }
        return JsonUtil.getSuccessJsonObject();
    }

    /**
     * 实时交易
     */
    @Async("taskExecutor")
    public void realTransaction() {
        if (!"transaction".equals(active)) return;
        logger.info("运行：实时交易");
        while (true) {
            logger.debug("\n\n\n");
            try {
                List<Strategy> strategyList = strategyService.selectListAll();
                List<StrategyUser> strategyUsers = strategyUserService.selectListAll();
                List<Token> tokens = tokenService.selectListAll();
                for (Token token : tokens) {
                    if (token.getTestEnable()) continue;//过滤测试账号
                    logger.debug("///////////////////////" + token.getUsername() + " start///////////////////////");
                    for (StrategyUser strategyUser:strategyUsers){
                        if (!strategyUser.getUsername().equals(token.getUsername()))continue;
                        if(!strategyUser.getEnable())continue;
                        for(Strategy strategy:strategyList){
                            if(!strategyUser.getStrategyId().equals(strategy.getId()))continue;
                            logger.debug("\n");
                            if ("BTC".equals(strategy.getSymbol())) {
                                if ("buy".equals(strategy.getBuySell())) {
                                    StrategyUtil.buyBTC(url_prex,strategy, strategyUser, token);
                                }else if("sell".equals(strategy.getBuySell())){
                                    StrategyUtil.sellBTC(url_prex,strategy, strategyUser, token);
                                }
                            }
                        }
                    }
                    logger.debug("|||||||||||||||||||||| = " + token.getUsername() + " end = ||||||||||||||||||||||\n");
                }
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("执行实时交易异常!!!");
            }
        }
    }
}